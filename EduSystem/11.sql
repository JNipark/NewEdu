USE [master]
GO
/****** Object:  Database [EduSystem]    Script Date: 2020/4/4 17:02:20 ******/
CREATE DATABASE [EduSystem] ON  PRIMARY 
( NAME = N'EduSystem', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\EduSystem.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EduSystem_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\EduSystem_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EduSystem] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EduSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EduSystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EduSystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EduSystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EduSystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EduSystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [EduSystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EduSystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EduSystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EduSystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EduSystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EduSystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EduSystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EduSystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EduSystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EduSystem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EduSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EduSystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EduSystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EduSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EduSystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EduSystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EduSystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EduSystem] SET RECOVERY FULL 
GO
ALTER DATABASE [EduSystem] SET  MULTI_USER 
GO
ALTER DATABASE [EduSystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EduSystem] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'EduSystem', N'ON'
GO
USE [EduSystem]
GO
/****** Object:  User [zqSql]    Script Date: 2020/4/4 17:02:20 ******/
CREATE USER [zqSql] FOR LOGIN [zqSql] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[tb_Question]    Script Date: 2020/4/4 17:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Question](
	[VideoNo] [nvarchar](max) NULL,
	[Question] [nvarchar](max) NULL,
	[AnswerA] [nvarchar](max) NULL,
	[AnswerB] [nvarchar](max) NULL,
	[AnswerC] [nvarchar](max) NULL,
	[RightAnswer] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_Session]    Script Date: 2020/4/4 17:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Session](
	[Session] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[UserName] [nvarchar](max) NULL,
	[Rank] [nvarchar](max) NULL,
	[CreateTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_UserInfo]    Script Date: 2020/4/4 17:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_UserInfo](
	[UserName] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[Rank] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_UserVideo]    Script Date: 2020/4/4 17:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_UserVideo](
	[UserPhone] [nvarchar](max) NULL,
	[VideoNo] [nvarchar](max) NULL,
	[Statement] [nvarchar](max) NULL,
	[QuestionNo] [nvarchar](max) NULL,
	[QuestionAns] [nvarchar](max) NULL,
	[UpdateTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_VideoInfo]    Script Date: 2020/4/4 17:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_VideoInfo](
	[VideoNo] [nvarchar](max) NULL,
	[VideoName] [nvarchar](max) NULL,
	[VideoPath] [nvarchar](max) NULL,
	[CreateTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[tb_Question] ([VideoNo], [Question], [AnswerA], [AnswerB], [AnswerC], [RightAnswer]) VALUES (N'测试视频.mp4', N'测试题目1', N'A', N'B', N'C', N'A')
INSERT [dbo].[tb_Question] ([VideoNo], [Question], [AnswerA], [AnswerB], [AnswerC], [RightAnswer]) VALUES (N'测试视频.mp4', N'测试题目1', N'A', N'B', N'C', N'A')
INSERT [dbo].[tb_Question] ([VideoNo], [Question], [AnswerA], [AnswerB], [AnswerC], [RightAnswer]) VALUES (N'TestVideo.mp4', N'测试题目2', N'A', N'B', N'C', N'A')
INSERT [dbo].[tb_Session] ([Session], [PhoneNumber], [UserName], [Rank], [CreateTime]) VALUES (N'8f64a5654f57d8ab42a5fd350f10b376', N'18051825670', N'仲庆', N'5', CAST(N'2020-03-14T10:28:36.673' AS DateTime))
INSERT [dbo].[tb_Session] ([Session], [PhoneNumber], [UserName], [Rank], [CreateTime]) VALUES (N'd78a963ebc03a33ff1f059838082e752', N'18051825670', N'仲庆', N'5', CAST(N'2020-03-21T16:54:55.143' AS DateTime))
INSERT [dbo].[tb_Session] ([Session], [PhoneNumber], [UserName], [Rank], [CreateTime]) VALUES (N'4bb1602d7ece4e93bfb7196db4e4fd8b', N'18915609618', N'仲锋', N'5', CAST(N'2020-03-21T18:33:52.427' AS DateTime))
INSERT [dbo].[tb_Session] ([Session], [PhoneNumber], [UserName], [Rank], [CreateTime]) VALUES (N'91a5a0f0881a122a5aff2a80189d07f5', N'18051825670', N'仲庆', N'5', CAST(N'2020-03-29T20:32:41.653' AS DateTime))
INSERT [dbo].[tb_Session] ([Session], [PhoneNumber], [UserName], [Rank], [CreateTime]) VALUES (N'fd07b488b444a1817401b2553e6e91d7', N'18051825670', N'仲庆', N'5', CAST(N'2020-03-30T22:56:32.793' AS DateTime))
INSERT [dbo].[tb_UserInfo] ([UserName], [PhoneNumber], [Password], [Rank]) VALUES (N'仲庆', N'18051825670', N'b97b8c9c7bff6cdb7969288d4d0bcc96', N'5')
INSERT [dbo].[tb_UserInfo] ([UserName], [PhoneNumber], [Password], [Rank]) VALUES (N'仲锋', N'18915609618', N'e10adc3949ba59abbe56e057f20f883e', N'5')
INSERT [dbo].[tb_UserVideo] ([UserPhone], [VideoNo], [Statement], [QuestionNo], [QuestionAns], [UpdateTime]) VALUES (N'18051825670', N'测试视频.mp4', N'否', N'测试题目1', N'A', CAST(N'2020-03-21T18:17:29.227' AS DateTime))
INSERT [dbo].[tb_UserVideo] ([UserPhone], [VideoNo], [Statement], [QuestionNo], [QuestionAns], [UpdateTime]) VALUES (N'18051825670', N'TestVideo.mp4', N'否', N'测试题目2', N'A', CAST(N'2020-03-21T18:18:32.893' AS DateTime))
INSERT [dbo].[tb_UserVideo] ([UserPhone], [VideoNo], [Statement], [QuestionNo], [QuestionAns], [UpdateTime]) VALUES (N'18915609618', N'测试视频.mp4', N'否', N'测试题目1', N'A', CAST(N'2020-03-21T18:33:31.660' AS DateTime))
INSERT [dbo].[tb_UserVideo] ([UserPhone], [VideoNo], [Statement], [QuestionNo], [QuestionAns], [UpdateTime]) VALUES (N'18915609618', N'测试视频.mp4', N'否', N'测试题目1', N'A', CAST(N'2020-03-21T18:33:31.660' AS DateTime))
INSERT [dbo].[tb_UserVideo] ([UserPhone], [VideoNo], [Statement], [QuestionNo], [QuestionAns], [UpdateTime]) VALUES (N'18915609618', N'TestVideo.mp4', N'否', N'测试题目2', N'A', CAST(N'2020-03-21T18:33:31.660' AS DateTime))
INSERT [dbo].[tb_VideoInfo] ([VideoNo], [VideoName], [VideoPath], [CreateTime]) VALUES (N'测试视频.mp4', NULL, N'http://127.0.0.1:8001/VideoPath/测试视频.mp4', CAST(N'2020-03-21T18:17:01.367' AS DateTime))
INSERT [dbo].[tb_VideoInfo] ([VideoNo], [VideoName], [VideoPath], [CreateTime]) VALUES (N'TestVideo.mp4', NULL, N'http://127.0.0.1:8001/VideoPath/TestVideo.mp4', CAST(N'2020-03-21T18:18:21.187' AS DateTime))
INSERT [dbo].[tb_VideoInfo] ([VideoNo], [VideoName], [VideoPath], [CreateTime]) VALUES (N'20200321.mp4', NULL, N'http://192.168.2.184:8001/VideoPath/20200321.mp4', CAST(N'2020-03-21T18:19:26.987' AS DateTime))
/****** Object:  StoredProcedure [dbo].[IPS_ModifyInDate]    Script Date: 2020/4/4 17:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[IPS_ModifyInDate]
    @LicensePlateNo nvarchar(20),
    @InDate Int
As
Begin
	--定义错误编号
	Declare @ErrorID SmallInt
    
	Set @ErrorID = 1000
    Begin Try 
        Begin Tran IPS_ModifyInDateTran
  
            Update IPS_Order Set InDate = @InDate where LicensePlateNo = @LicensePlateNo
            
        Commit Tran IPS_ModifyInDateTran
            
    End Try
	Begin Catch
		Set @ErrorID = 1001
	End Catch
    
    If @@TRANCOUNT > 0 
    Begin
        Set @ErrorID = 1002
        RollBack Tran IPS_ModifyInDateTran
    End

	Return @ErrorID
End
GO
/****** Object:  StoredProcedure [dbo].[IPS_SaveOrder_local]    Script Date: 2020/4/4 17:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[IPS_SaveOrder_local]
    @DataType Int,
    @XMLData Nvarchar(Max),
    @OrderNoReturn nvarchar(50) output,
    @BerthNum int output, --剩余车位
    @ChargeTypeReturn tinyint output,
    @MonthLeftDayReturn int output
As
Begin
	--定义错误编号
	Declare @ErrorID SmallInt
    Declare @I Int
    Declare @MaxID Int
    Declare @MaxPictureId Int
    Declare @ChargeType tinyint
    Declare @ParkingLotId int
    Declare @OrderNo nvarchar(50)
    Declare @State tinyint
    Declare @LicensePlateNo nvarchar(20)
    Declare @ParkingLotNo nvarchar(40)
    Declare @OrderIdHistroy int 
    Declare @MonthLeftDay int
    Declare @InDate datetime
    Declare @CarNum int
    Declare @Note nvarchar(1000)
    Declare @PayDetailId int
    Declare @OutDate datetime
    Declare @ParkingTime int
    Declare @OrderCharge decimal(9,2)
    Declare @ActualGetAmount decimal(9,2)
    Declare @ActualAmount decimal(9,2)
    Declare @DiscountAmount decimal(9,2)
    Declare @Paytime datetime
    Declare @CarType tinyint
    Declare @LicensePlateType tinyint
    Declare @MemberId int
    Declare @PayMoney decimal(9,2)
    Declare @RoadRateNo nvarchar(20)
    Declare @InMinute int
    Declare @InDiffMinute int
    Declare @IsCreateOrder tinyint
    Declare @OrderNo_Old nvarchar(50)
    Declare @InDate_Old datetime
    Declare @LimitWeek tinyint
    Declare @OrderComplete int
    Declare @ChargeRuleId int
    Declare @TmpChargeRuleId int
    Declare @InStartTime int
    Declare @InEndTime int
    Declare @MothlyTimeId int
    Declare @TmpOrder Table(
            OrderNo nvarchar(50),
            OrderType tinyint,
            ParkingLotId int,
            BerthNo nvarchar(30),
            MemberId int,
            LicensePlateNo nvarchar(20),
            LicensePlateType tinyint,
            InDate datetime,
            OutDate datetime,
            ParkingTime int,
            OrderCharge decimal(9,2),
            ActualAmount decimal(9,2),
            ActualGetAmount decimal(9,2),
            DiscountAmount decimal(9,2),
            OrderComplete int,
            OrderCompleteDate datetime,
            State tinyint,
            Note nvarchar(1000),
            ChargeType tinyint,
            RoadRateNo nvarchar(20),
            InMinute int,  --入场间隔
            CreateEmp int,
            EntranceNo nvarchar(20)
            )            
    Declare @TmpOrderPic Table(
			PictureId Int,
            PictureAddr nvarchar(200),
            PictureName nvarchar(50),
            PicType tinyint,
            OrderNo nvarchar(50),
            Note nvarchar(1000),
            SizeMode tinyint
            )    
   Declare @TmpOrderPayDetail Table(
			PayDetailId int,
            OrderNo nvarchar(50),
            PayNo nvarchar(50),
            PayType tinyint,
            PayMoney decimal(9,2),
            ChargeEmp int,
            Note nvarchar(1000)
            )    
    Declare @TmpChargeRule Table(
			ChargeRuleId int,
            InStartTime int,
            InEndTime int
            )                      
	Set @ErrorID = 1000
    Set @OrderNoReturn = ''
    Set @BerthNum = 0 --剩余车位
    Set @ChargeTypeReturn = 10
    Set @MonthLeftDayReturn = 0
    Set @ChargeRuleId = 0
    Begin Try 
     Begin Tran IPS_SaveOrderTran  
            If @DataType = 1  --新增，生成入场订单
            Begin
                /**********************解析XML文件***************************/
                EXEC sp_xml_preparedocument @I OUTPUT,@XMLData
                Set @State = 10
                Insert into @TmpOrder
        		SELECT	
                        ISNULL(OrderNo,''),
                        ISNULL(OrderType,0),
                        ISNULL(ParkingLotId,0),
                        ISNULL(BerthNo,''),
                        ISNULL(MemberId,0),
                        ISNULL(LicensePlateNo,''),
                        ISNULL(LicensePlateType,1),
                        InDate,
                        OutDate,
                        ISNULL(ParkingTime,0),
                        ISNULL(OrderCharge,0),
                        ISNULL(ActualAmount,0),
                        ISNULL(ActualGetAmount,0),
                        ISNULL(DiscountAmount,0),
                        ISNULL(OrderComplete,0),
                        OrderCompleteDate,
                        ISNULL(State,10),
                        ISNULL(Note,''),
                        ISNULL(ChargeType,0),
                        ISNULL(RoadRateNo,''),
                        ISNULL(InMinute,0),
                        ISNULL(CreateEmp,0),
                        ISNULL(EntranceNo,'')
    			FROM OPENXML(@I, '/IPS_Order/Rows', 2)
    			With(OrderNo nvarchar(50),OrderType tinyint,ParkingLotId int,BerthNo nvarchar(30),MemberId int,LicensePlateNo nvarchar(20),
                    LicensePlateType tinyint,InDate datetime,OutDate datetime,ParkingTime int,OrderCharge decimal(9,2),ActualAmount decimal(9,2),
                    ActualGetAmount decimal(9,2),DiscountAmount decimal(9,2),OrderComplete int,OrderCompleteDate datetime,State tinyint,
                    Note nvarchar(1000),ChargeType tinyint,RoadRateNo nvarchar(20),InMinute int,CreateEmp int,EntranceNo nvarchar(20))
                    
                Insert into @TmpOrderPic
        		SELECT	ISNULL(PictureId,0),
                        ISNULL(PictureAddr,''),
                        ISNULL(PictureName,''),
                        ISNULL(PicType,1),  --入场图片
                        ISNULL(OrderNo,''), 
                        ISNULL(Note,''),
                        ISNULL(SizeMode,1)
    			FROM OPENXML(@I, '/IPS_Order/Rows/IPS_OrderPic/Rows', 2)
    			With(PictureId Int,PictureAddr nvarchar(200),PictureName nvarchar(50),PicType tinyint,OrderNo nvarchar(50),Note nvarchar(1000),SizeMode tinyint)    
    			EXEC sp_xml_removedocument @I
                /***********************************************************/  
                Select @ParkingLotId = A.ParkingLotId,@LicensePlateNo = A.LicensePlateNo,@LicensePlateType = LicensePlateType,@InDate = A.InDate,@ParkingLotNo = B.ParkingLotNo,@MemberId = MemberId,
                    @InMinute = InMinute,@InDate = InDate From @TmpOrder A Inner Join IPS_ParkingLot B On A.ParkingLotId = B.ParkingLotId
                Set @OrderNo = 'D' + @ParkingLotNo + CONVERT(varchar(100), @InDate, 112)+replace(CONVERT(varchar(100), @InDate, 8),':','') + Left(NEWID(),5)
                Set @MonthLeftDay = 0
                Set @MonthLeftDayReturn = 0
                Set @CarType = 1
                Set @IsCreateOrder = 1  --需要创建订单
                If CHARINDEX('学',@LicensePlateNo) = 0  --非教练车并且为黄牌车
                Begin
                    If @LicensePlateType = 2 Or @LicensePlateType = 5 --牌照颜色为黄牌，黄绿认为是大型车
                        Set @CarType = 2
                End  
                      
                If @LicensePlateType = 6
                Begin
                    --如果是新能源车，判断是否有新能源车的收费规则
                    If Exists(Select A.* from IPS_ChargeConfig A with(nolock) Inner Join IPS_ChargeRule B with(nolock) On B.ChargeRuleId = A.ChargeRuleId Where A.ParkingLotId = @ParkingLotId And B.LicensePlateType = 4 And B.State = 1 And A.State = 1)
                        Set @CarType = 4
                End        
                
                If Exists(Select OrderId From IPS_Order Where LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId And State in (10,15))
                Begin
                    
                    Select @OrderNo_Old = Max(OrderNo) From IPS_Order Where LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId And State in (10,15)
                    Select @InDate_Old = InDate From IPS_Order Where OrderNo = @OrderNo_Old
                    If DateDiff(MI,@InDate_Old,@InDate) <= @InMinute   --比较两次入场时间间隔，判断是否需要重新生成订单
                    Begin
                        Set @IsCreateOrder = 0    --不需要创建
                    End     
                    Else
                    Begin
                        --异常订单插入到历史表
                        Set @OrderIdHistroy = IsNull((Select MAX(OrderId) From IPS_OrderHistroy),0)
                    
                        Insert into  IPS_OrderHistroy(OrderId,OrderNo,OrderType,ParkingLotId,BerthNo,MemberId,LicensePlateNo,LicensePlateType,InDate,OutDate,ParkingTime,OrderCharge,ActualAmount,ActualGetAmount,DiscountAmount,
                        OrderComplete,OrderCompleteDate,State,Note,ChargeType,CarType,Paytime,RoadRateNo,CreateEmp,ChargeRuleId,EntranceNo)
                        Select ROW_NUMBER() Over(Order By OrderNo) + @OrderIdHistroy ,OrderNo,OrderType,ParkingLotId,BerthNo,MemberId,LicensePlateNo,LicensePlateType,
                            InDate,OutDate,ParkingTime,OrderCharge,ActualAmount,ActualGetAmount,DiscountAmount,OrderComplete,OrderCompleteDate,40,'出场异常',ChargeType,
                            CarType,null,RoadRateNo,CreateEmp,ChargeRuleId,EntranceNo
                            From  IPS_Order Where OrderId in (Select OrderId From IPS_Order Where LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId And State in (10,15))
                    
                        Delete From  IPS_Order Where OrderId in (Select OrderId From IPS_Order Where LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId And State in (10,15))
                    End  
                End   
                
                If @IsCreateOrder = 1  --创建订单
                Begin
                    Set @ChargeType = 10 --正常收费
                    --判断是否特种车辆（白色牌照）
                    If (@LicensePlateType = 7) 
                        Set @ChargeType = 20 --白名单车辆
                    Else
                    Begin    
                        --判断是否白名单并且在有效时间内
                        If not Exists(Select A.WhiteListId from IPS_WhiteList A Inner Join IPS_WhiteListBindParkingLot B 
                                    On A.WhiteListId = B.WhiteListId And A.State = 1
                                    Where B.ParkingLotId = @ParkingLotId And A.LicensePlateNo = @LicensePlateNo
                                    And DateAdd(day,A.ValidDay,A.RegisterDate) >= GetDate() And State = 1)
                        Begin
                            --判断是否包月用户
                            If Exists(Select MonthlyId from IPS_ParkingLotMonthly_View Where LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId)
                            Begin
                                --如果有多条包月纪录，默认返回第一条
                                Select @MonthLeftDay = DateDiff(day,GetDate(),tmp.EndDate),@MothlyTimeId = MothlyTimeId  From (	
                                	Select MonthlyId,MothlyTimeId,EndDate,ROW_NUMBER() Over(Order By EndDate desc) sort
                                		From IPS_ParkingLotMonthly_View Where LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId
                                		)tmp
                                	where tmp.sort = 1	 
                                --Select @MonthLeftDay = DateDiff(day,GetDate(),EndDate) From IPS_ParkingLotMonthly_View Where LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId
                                If @MonthLeftDay > 0 
                                Begin
                                    Set @ChargeType = 30  
                                    Set @MonthLeftDayReturn = @MonthLeftDay
                                End   
                            End
                            Else
                                Set @MothlyTimeId = 0
                                                       
                        End
                        Else
                        Begin
                            --白名单车辆
                           Set @ChargeType = 20
                            --判断这一天是否限行
                            Set @LimitWeek = (Case (Select DATENAME(DW,GETDATE())) When '星期一' Then 1 When '星期二' Then 2 When '星期三' Then 3 When '星期四' Then 4 When '星期五' Then 5 When '星期六' Then 6 when '星期日' Then 7 End)
                            If Exists(Select * From IPS_WeekLimitTrafficWhiteList Where LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId And LimitTrafficWeek = @LimitWeek)
                            Begin
                                --入场时间在新区医院白名单限行车辆规则内的按照该规则收费，否则还是白名单
                                Declare @InDateMinute int    --入场时间转换成分钟数
                                Declare @StartTime_Limit int
                                Declare @EndTime_Limit int
                                Select @StartTime_Limit = ISNULL(MIN(StartTime),0),@EndTime_Limit = ISNULL(MAX(EndTime),0) from IPS_ChargeRule Where LicensePlateType = 5 And State = 1		
                                Set @InDateMinute = DATEPART(hh,@InDate) * 60 + DATEPART(mi,@InDate) + (Case when DATEPART(ss,@InDate) > 0 then 1 Else 0 End)
                                If @InDateMinute >= @StartTime_Limit And @InDateMinute <= @EndTime_Limit 
                                Begin
                                    Set @ChargeType = 10   --限行车辆需要计费
                                    Set @CarType = 5       --限行车辆计费规则
                                End
                            End    
                        End   
                    End       
                    If @ChargeType = 10  --临停车计费规则绑定
                    Begin
                        --根据CarType和入场时间获取ChargeRuleId
                        If Exists(Select A.* from IPS_ChargeConfig A with(nolock) 
                                	Inner Join IPS_ChargeRule B with(nolock) On B.ChargeRuleId = A.ChargeRuleId 
                                	Where A.ParkingLotId = @ParkingLotId And B.LicensePlateType = @CarType And B.State = 1 And A.State = 1)
                        Begin
                        
                            Insert into @TmpChargeRule
                            Select B.ChargeRuleId,InStartTime,InEndTime from IPS_ChargeConfig A with(nolock) 
                                	Inner Join IPS_ChargeRule B with(nolock) On B.ChargeRuleId = A.ChargeRuleId 
                                	Where A.ParkingLotId = @ParkingLotId And B.LicensePlateType = @CarType And B.State = 1 And A.State = 1
                            If (Select Count(1) From @TmpChargeRule) = 1 
                                Select @ChargeRuleId = ChargeRuleId From @TmpChargeRule
                            Else
                            Begin
                                --某一车型有多个收费规则要根据入场时间来判断
                                While (exists (select 1 from @TmpChargeRule))
                                Begin
                                    Select top 1 @TmpChargeRuleId = ChargeRuleId,@InStartTime = InStartTime,@InEndTime = InEndTime from @TmpChargeRule 
                                    Set @InDateMinute = DATEPART(hh,@InDate) * 60 + DATEPART(mi,@InDate) + (Case when DATEPART(ss,@InDate) > 0 then 1 Else 0 End)
                                    If @InStartTime > @InEndTime   --跨天的情况
                                    Begin 
                                        Set @InEndTime = @InEndTime + 1440
                                        If @InDateMinute >= @InStartTime And @InDateMinute <= @InEndTime
                                        Begin
                                            Set @ChargeRuleId = @TmpChargeRuleId  
                                            Break
                                        End
                                        Else
                                            Set @InDateMinute =  @InDateMinute + 1440   
                                    End
                                    If @InDateMinute >= @InStartTime And @InDateMinute <= @InEndTime 
                                    Begin
                                        Set @ChargeRuleId = @TmpChargeRuleId  
                                        Break
                                    End    
                                    DELETE @TmpChargeRule WHERE ChargeRuleId = @TmpChargeRuleId
                                End  
                            End        
                        
                        End  
                        --计算ChargeRuleId结束 
                    End   
                    --计算分时包月的ChargeRuleId
                    Else If @ChargeType = 30  --包月车辆
                    Begin
                        --包月车辆有计费规则，即分时包月
                        If @MothlyTimeId <> 0
                        Begin
                            If Exists(Select * from IPS_ParkingLotMothlyTime with(nolock) Where MothlyTimeId = @MothlyTimeId)    
                                Select @ChargeRuleId = ChargeRuleId From IPS_ParkingLotMothlyTime where  MothlyTimeId = @MothlyTimeId    
                        End
                    End      
                    
                    Set @MaxID = IsNull((Select MAX(OrderId) From IPS_Order),0) + 1
                    Insert into IPS_Order(OrderId,OrderNo,OrderType,ParkingLotId,BerthNo,MemberId,LicensePlateNo,LicensePlateType,InDate,OutDate,ParkingTime,OrderCharge,ActualAmount,ActualGetAmount,DiscountAmount,
                        OrderComplete,OrderCompleteDate,State,Note,ChargeType,CarType,Paytime,RoadRateNo,CreateEmp,ChargeRuleId,EntranceNo)
                    Select @MaxID,@OrderNo,OrderType,ParkingLotId,BerthNo,MemberId,LicensePlateNo,LicensePlateType,InDate,null,ParkingTime,OrderCharge,ActualAmount,ActualGetAmount,DiscountAmount,OrderComplete,null,
                        State,'',@ChargeType,@CarType,null,RoadRateNo,CreateEmp,@ChargeRuleId,EntranceNo 
                        From @TmpOrder
                  
                    Set @MaxPictureId = IsNull((Select MAX(PictureId) From IPS_OrderPic),0)
               
                    Insert into IPS_OrderPic
                    Select ROW_NUMBER() Over(Order By PictureId) + @MaxPictureId,PictureAddr,PictureName,PicType,@OrderNo,@InDate,1,Note,SizeMode From @TmpOrderPic
                    Set @OrderNoReturn = @OrderNo
                    --计算余位
                    Select @CarNum = Count(tmp.LicensePlateNo) from
                        (Select  Distinct LicensePlateNo From IPS_Order Where State = 10 And ParkingLotId = @ParkingLotId) tmp
                    Select @BerthNum = (BerthNum -  @CarNum) From IPS_ParkingLot Where ParkingLotId = @ParkingLotId
                
                    Set @ChargeTypeReturn = @ChargeType
                End
                Else
                Begin
                    Set @OrderNoReturn = @OrderNo_Old
                    Set @ErrorId = 1004 --不需要创建订单
                End    

            End
         
            If @DataType = 2  --修改订单计算收费金额
            Begin
                /**********************解析XML文件***************************/
                EXEC sp_xml_preparedocument @I OUTPUT,@XMLData
        		SELECT	
                        @LicensePlateNo = LicensePlateNo,
                        @OutDate = OutDate,
                        @ParkingLotId = ParkingLotId,
                        @LicensePlateType = LicensePlateType,
                        @RoadRateNo = ISNULL(RoadRateNo,'')
    			FROM OPENXML(@I, '/IPS_Order/Rows', 2)
    			With(LicensePlateNo nvarchar(20),OutDate datetime,ParkingLotId int,LicensePlateType tinyint,RoadRateNo nvarchar(20))
                    
                Insert into @TmpOrderPic
        		SELECT	ISNULL(PictureId,0),
                        ISNULL(PictureAddr,''),
                        ISNULL(PictureName,''),
                        ISNULL(PicType,2),  --出场图片
                        ISNULL(OrderNo,''),
                        ISNULL(Note,''),
                        ISNULL(SizeMode,1)
    			FROM OPENXML(@I, '/IPS_Order/Rows/IPS_OrderPic/Rows', 2)
    			With(PictureId Int,PictureAddr nvarchar(200),PictureName nvarchar(50),PicType tinyint,OrderNo nvarchar(50),Note nvarchar(1000),SizeMode tinyint)    
 
    			EXEC sp_xml_removedocument @I
                /***********************************************************/  
                
                
                Set @ParkingTime = 0
                Set @OrderCharge = 0
                Set @ActualGetAmount = 0
                Set @ActualAmount = 0
                Set @DiscountAmount = 0
                
                
                
                If Exists(Select OrderNo From IPS_Order Where State = 10 And LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId)
                Begin
                    
                    --读取该车牌的最近一张进行中的订单Id
                    Select @OrderNo = OrderNo
                        from (Select OrderId,OrderNo,ROW_NUMBER() Over(Order By InDate desc) sort 
                                From IPS_Order Where State = 10 And LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId)tmp
                        where tmp.sort = 1
                    
                    --读取订单费用
                    exec IPS_GetOrderFee @OrderNo,@OutDate,@OrderCharge output,@DiscountAmount output,@ActualAmount output,@ParkingTime output 
                    
                    Update IPS_Order Set OutDate = @OutDate,ParkingTime = @ParkingTime,OrderCharge = @OrderCharge,ActualAmount = @ActualAmount,DiscountAmount = @DiscountAmount,RoadRateNo = @RoadRateNo
                            Where OrderNo = @OrderNo
                            
                    --保存出场图片
                    Set @MaxPictureId = IsNull((Select MAX(PictureId) From IPS_OrderPic),0)  
                    Insert into IPS_OrderPic
                        Select ROW_NUMBER() Over(Order By PictureId) + @MaxPictureId,PictureAddr,PictureName,PicType,@OrderNo,@OutDate,1,Note,SizeMode From @TmpOrderPic
                    
                    Set @OrderNoReturn = @OrderNo
                End
                Else
                    Set @ErrorID = 1003  --该车无可用订单，没有入场纪录
                
            End     
           
             
           If @DataType = 3 --出场更新订单信息 
            Begin
                 /**********************解析XML文件***************************/
                EXEC sp_xml_preparedocument @I OUTPUT,@XMLData
                
                SELECT	
                        @OrderNo = OrderNo,
                        @ParkingTime = ParkingTime,
                        @OrderCharge = OrderCharge,
                        @ActualAmount = ActualAmount,
                        @DiscountAmount = DiscountAmount,
                        @Paytime = Paytime,
                        @Note = Note,
                        @ActualGetAmount = ActualGetAmount,
                        @OrderComplete = OrderComplete
    			FROM OPENXML(@I, '/IPS_Order/Rows', 2)
    			With(OrderNo nvarchar(50),ParkingTime int,OrderCharge decimal(9,2),ActualAmount decimal(9,2),DiscountAmount decimal(9,2),Paytime nvarchar(20),
                    Note nvarchar(1000),ActualGetAmount decimal(9,2),OrderComplete int)
                
                Insert into @TmpOrderPayDetail
        		SELECT	ISNULL(PayDetailId,0),
                        ISNULL(OrderNo,''),
                        ISNULL(PayNo,''),
                        ISNULL(PayType,1),
                        ISNULL(PayMoney,0),
                        ISNULL(ChargeEmp,0),
                        ISNULL(Note,'')
    			FROM OPENXML(@I, '/IPS_Order/Rows/IPS_OrderPayDetail/Rows', 2)
    			With(PayDetailId Int,OrderNo nvarchar(50),PayNo nvarchar(50),PayType tinyint,PayMoney decimal(9,2),ChargeEmp int,Note nvarchar(1000))   
    			EXEC sp_xml_removedocument @I
                /***********************************************************/               
                Select @ParkingLotId = ParkingLotId From IPS_Order Where OrderNo = @OrderNo
                Set @PayMoney = 0
                Set @State = 30
                
                If Exists(Select * from  @TmpOrderPayDetail)
                Begin
					Set @Paytime = GETDATE()
                    Select @PayMoney = PayMoney From @TmpOrderPayDetail 
                End    
   
                Set @OrderIdHistroy = IsNull((Select MAX(OrderId) From IPS_OrderHistroy),0) + 1
                               
                Insert into  IPS_OrderHistroy(OrderId,OrderNo,OrderType,ParkingLotId,BerthNo,MemberId,LicensePlateNo,LicensePlateType,InDate,OutDate,ParkingTime,OrderCharge,ActualAmount,ActualGetAmount,DiscountAmount,
                    OrderComplete,OrderCompleteDate,State,Note,ChargeType,CarType,Paytime,RoadRateNo,CreateEmp,ChargeRuleId,EntranceNo)
                Select ROW_NUMBER() Over(Order By OrderNo) + @OrderIdHistroy ,OrderNo,OrderType,ParkingLotId,BerthNo,MemberId,LicensePlateNo,LicensePlateType,
                    InDate,OutDate,@ParkingTime,@OrderCharge,@ActualAmount,@ActualGetAmount + @PayMoney,@DiscountAmount,@OrderComplete,GetDate(),@State,@Note,
                    ChargeType,CarType,@Paytime,RoadRateNo,CreateEmp,ChargeRuleId,EntranceNo 
                    From  IPS_Order Where OrderNo = @OrderNo 
                    
                Delete From  IPS_Order Where OrderNo = @OrderNo  
                 --插入支付方式,本地的只有现金才需要支付纪录
                Set @PayDetailId = IsNull((Select MAX(PayDetailId) From IPS_OrderPayDetail),0) + 1  
                Insert into IPS_OrderPayDetail
                Select @PayDetailId,OrderNo,PayNo,@Paytime,PayType,PayMoney,ChargeEmp,Note  From @TmpOrderPayDetail    
                
                --出场后计算余位
               Select @CarNum = Count(tmp.LicensePlateNo) from
                    (Select  Distinct LicensePlateNo From IPS_Order Where State = 10 And ParkingLotId = @ParkingLotId) tmp
               Select @BerthNum = (BerthNum -  @CarNum) From IPS_ParkingLot Where ParkingLotId = @ParkingLotId     
           
                  
            End
            
            If @DataType = 4 --修正车牌
            Begin
                
                EXEC sp_xml_preparedocument @I OUTPUT,@XMLData
 
        		SELECT	
                        @OrderNo = OrderNo,
                        @LicensePlateNo = LicensePlateNo
                FROM OPENXML(@I, '/IPS_Order/Rows', 2)         
    			With(OrderNo nvarchar(50),LicensePlateNo nvarchar(20))  
    			EXEC sp_xml_removedocument @I    
                If not Exists(Select OrderNo From IPS_Order Where LicensePlateNo = @LicensePlateNo)
                Begin
                    Declare @ChargeTypeCheck tinyint  --车辆计费类别
                        Set @ChargeTypeCheck = 0
                    Select @ParkingLotId = ParkingLotId From IPS_Order where OrderNo = @OrderNo
                    --判断车辆是否是白名单
                    If Exists(Select * From IPS_WhiteList A Inner Join IPS_WhiteListBindParkingLot B On B.WhiteListId = A.WhiteListId 
                        Where A.LicensePlateNo = @LicensePlateNo And B.ParkingLotId = @ParkingLotId And A.State = 1)   
                        Set @ChargeTypeCheck = 1   --白名单车辆
                        
                    Else If Exists(Select * from IPS_ParkingLotMonthly_View Where LicensePlateNo = @LicensePlateNo And ParkingLotId = @ParkingLotId)
                        Set @ChargeTypeCheck = 2   --包月车辆
                    Update IPS_Order Set LicensePlateNo = @LicensePlateNo,ChargeType = (Case @ChargeTypeCheck When 0 Then ChargeType when 1 then 20 when 2 then 30 End) Where OrderNo = @OrderNo
                End
                Else
                    Set @ErrorID = 1005  --该车已存在，请确认是是否有重复识别的情况并标记成异常
            End 
            
             
            
        Commit Tran IPS_SaveOrderTran    
    End Try
	Begin Catch
		Set @ErrorID = 1001
	End Catch
    
    If @@TRANCOUNT > 0 
    Begin
        Set @ErrorID = 1002
        RollBack Tran IPS_SaveOrderTran
    End
    
	Return @ErrorID
End
GO
/****** Object:  StoredProcedure [dbo].[IPS_SyncParkingLotBaseData]    Script Date: 2020/4/4 17:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[IPS_SyncParkingLotBaseData]
    @DataType Int,
    @XMLData Nvarchar(Max)
As
Begin
	--定义错误编号
	Declare @ErrorID SmallInt
    Declare @I Int
    Declare @ParkingLotId int
    Declare @UserId int
    Declare @MonthlyId Nvarchar(40)
    Declare @SystemId int
    Declare @ChargeRuleId int
    Declare @ChargePlanId int
    Declare @WhiteListId int
    --停车场信息
    Declare @TmpParkingLot Table(
			ParkingLotId Int,
            ParkingLotNo Nvarchar(40),
            ParkingLotName Nvarchar(30),
            ParkingLotAddr Nvarchar(50),
            ParkingType Tinyint,
            ChargeType Tinyint,
            BerthNum int,
            ParkingFee Decimal(9,2),
            ChargeEmp int,
            RedTipNum int,
            YellowTipNum int,
            FreeTime int,
            Longitude decimal(18,7),
            Latitude decimal(18,7),
            Telphone Nvarchar(30),
            CreateDate datetime,
            CreateEmp int,
            ModifyDate datetime,
            ModifyEmp int,
            State tinyint,
            Note Nvarchar(1000),
            OrganizationId int
            )
    --白名单信息        
    Declare @TmpWhiteList Table(
			WhiteListId Int,
            LicensePlateNo Nvarchar(20),
            LicensePlateType tinyint,
            UserName nvarchar(20),
            RegisterDate datetime,
            OperateEmp int,
            State tinyint,
            Note Nvarchar(1000),
            ValidDay int)  
    Declare  @TmpWhiteListBindParkingLot  Table(
            SystemId  Int,
            WhiteListId  Int,
            ParkingLotId  Int)                
    --停车场包月相关
    Declare @TmpParkingLotMonthly Table(
			MonthlyId Int,
            StartDate datetime,
            EndDate datetime,
            OperateDate datetime,
            Amount decimal(18,2),
            MonthlyType tinyint,
            MemberId int,
            Opertator int,
            State tinyint,
            Note Nvarchar(1000),
            MothlyTimeId int)   
    Declare @TmpMothlyLicensePlate Table(
            SystemId Int,
			MonthlyId Int,
            LicensePlateNo nvarchar(20))   
    Declare @TmpMothlyGroup Table(
            SystemId Int,
			MonthlyId Int,
            ParkingLotId int)          
    --停车场收费规则相关
    Declare @TmpChargePlan Table(
			ChargePlanId Int,
            ChargePlanName Nvarchar(30),
            ChargePlanType tinyint,
            PlanStartDate datetime,
            PlanEndDate datetime,
            CreateEmp int,
            CreateTime datetime,
            ModifyEmp int,
            ModifyDate datetime,
            State tinyint,
            Note Nvarchar(1000)) 
    Declare @TmpChargeRule Table(
			ChargeRuleId Int,
            ChargeRuleName Nvarchar(30),
            ReferencePriceNote Nvarchar(30),
            LicensePlateType tinyint,
            ChargeMode tinyint,
            StartTime int,
            EndTime int,
            Freetype tinyint,
            Freetime int,
            TimeLimit int,
            CreateTime datetime,
            CreateEmp int,
            ModifyEmp int,
            ModifyDate datetime,
            ChargeLimit decimal(18,2),
            AttachCharge decimal(18,2),
            State tinyint,
            Note Nvarchar(1000),
            InStartTime int,
            InEndTime int
            )    
    Declare @TmpChargeRuleTime Table(
			SystemId Int,
            ChargeRuleId int,
            TimeType tinyint,
            StartTime int,
            EndTime int,
            UnitTime int,
            UnitCharge decimal(9,2),
            Note Nvarchar(1000))    
            
    Declare @TmpChargeConfig Table(
			ChargeConfigId Int,
            ChargePlanId Int,
            ChargeRuleId Int,
            ParkingLotId int,
            CarType tinyint,
            CreateEmp int,
            CreateTime datetime,
            ModifyEmp int,
            ModifyDate datetime,
            State tinyint,
            Note Nvarchar(1000)) 
    Declare @TmpUser Table(
			UserId Int,
            UserName Nvarchar(30),
            EmpId int,
            EmpName nvarchar(30),
            Phone Nvarchar(30),
            Email Nvarchar(30),
            Password Nvarchar(200),
            Note Nvarchar(1000)) 
    Declare @TmpUserParkingLot Table(
			SystemId Int,
            UserId int,
            ParkingLotId int)   
    --白名单限行列表        
    Declare @TmpWeekLimitTrafficWhiteList Table(
			Id Int,
            LimitTrafficWeek tinyint,
            WhiteListId int,
            ParkingLotId int,
            LicensePlateType tinyint,
            LicensePlateNo nvarchar(20),
            RegisterDate datetime,
            OperateEmp int,
            Note nvarchar(1000)
            )          
            
    Declare @TmpUser_Copy Table(
    		UserId int,
            UserName nvarchar(30),
            EmpId int,
            EmpName nvarchar(30),
            Phone nvarchar(30),
            Email Nvarchar(30),
            Password Nvarchar(200),
            Note nvarchar(200)) 
            
    --停车场分时包月时间段
    Declare @TmpParkingLotMothlyTime Table(
			MothlyTimeId Int,
            ParkingLotId int,
            StartMinute int,
            EndMinute int,
            State int,
            CreateEmp int,
            CreateTime datetime,
            ModifyEmp int,
            ModifyDate datetime,
            Note Nvarchar(1000),
            ChargeRuleId int)   
            
    --车辆分组
    Declare @TmpCarGroup Table(
			GroupId Int,
            ParkingLotId int,
            GroupName nvarchar(30),
            PId int,
            PName nvarchar(500),
            BerthNum int,
            CreateEmp int,
            CreateDate datetime,
            ModifyEmp int,
            ModifyDate datetime,
            State tinyint,
            Note Nvarchar(1000))    
            
    --车辆分组车牌号
    Declare @TmpCarGroupLicensePlateNo Table(
			SystemId Int,
            GroupId int,
            LicensePlateNo nvarchar(20))                   
            
    Declare @TmpMonthly_Copy Table(
    		MonthlyId int
            ) 
    Declare @TmpMothlyLicensePlate_Copy Table(
    		SystemId int
            ) 
    Declare @TmpMothlyGroup_Copy Table(
    		SystemId int
            )  
    Declare @TmpChargePlan_Copy Table(
			ChargePlanId Int
            )         
    Declare @TmpChargeRule_Copy Table(
    		ChargeRuleId int
            )              
            
    Declare @TmpChargeRuleTime_Copy Table(
    		SystemId int
            )   
    Declare @TmpWhiteList_Copy Table(
    		WhiteListId int
            )                                                                                                              
	Set @ErrorID = 1000
    Begin Try 
        Begin Tran IPS_SyncParkingLotBaseDataTran
            If @DataType = 1  --新增
            Begin
                /**********************解析XML文件***************************/
                EXEC sp_xml_preparedocument @I OUTPUT,@XMLData
            
                Insert into @TmpParkingLot
        		SELECT	ISNULL(ParkingLotId,0),
                        ISNULL(ParkingLotNo,''),
                        ISNULL(ParkingLotName,''),
                        ISNULL(ParkingLotAddr,''),
                        ISNULL(ParkingType,1),
                        ISNULL(ChargeType,1),
                        ISNULL(BerthNum,0),
                        ISNULL(ParkingFee,0),
                        ISNULL(ChargeEmp,0),
                        ISNULL(RedTipNum,0),
                        ISNULL(YellowTipNum,0),
                        ISNULL(FreeTime,0),
                        ISNULL(Longitude,0),
                        ISNULL(Latitude,0),
                        ISNULL(Telphone,''),
                        CreateDate,
                        ISNULL(CreateEmp,''),
                        ModifyDate,
                        ISNULL(ModifyEmp,0),
                        ISNULL(State,10),
                        ISNULL(Note,''),
                        ISNULL(OrganizationId,0)
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows', 2)
    			With(ParkingLotId Int,ParkingLotNo Nvarchar(40),ParkingLotName Nvarchar(30),ParkingLotAddr Nvarchar(50),ParkingType Tinyint,ChargeType Tinyint,
                    BerthNum int,ParkingFee Decimal(9,2),ChargeEmp int,RedTipNum int,YellowTipNum int,FreeTime int,Longitude decimal(18,7),Latitude decimal(18,7),
                    Telphone Nvarchar(30),CreateDate datetime,CreateEmp int,ModifyDate datetime,ModifyEmp int,State tinyint,Note Nvarchar(1000),OrganizationId int)
    			
                Insert into @TmpWhiteList
        		SELECT	ISNULL(WhiteListId,0),
                        ISNULL(LicensePlateNo,''),
                        ISNULL(LicensePlateType,0),
                        ISNULL(UserName,''),
                        RegisterDate,
                        ISNULL(OperateEmp,0),
                        ISNULL(State,1),
                        ISNULL(Note,''),
                        ISNULL(ValidDay,0)
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_WhiteList/Rows', 2)
    			With(WhiteListId Int,LicensePlateNo Nvarchar(20),LicensePlateType tinyint,UserName nvarchar(20),RegisterDate datetime,
                    OperateEmp int,State tinyint,Note Nvarchar(1000),ValidDay int)
                
                Insert into @TmpWhiteListBindParkingLot
        		SELECT	
        				ISNULL(SystemId,0),
        				ISNULL(WhiteListId,0),
                        ISNULL(ParkingLotId,0)
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_WhiteListBindParkingLot/Rows', 2)
    			With(SystemId int,WhiteListId Int,ParkingLotId int) 
                
                Insert into @TmpParkingLotMonthly
        		SELECT	ISNULL(MonthlyId,0),
                        StartDate,
                        EndDate,
                        OperateDate,
                        ISNULL(Amount,0),
                        ISNULL(MonthlyType,1),
                        ISNULL(MemberId,0),
                        ISNULL(Opertator,0),
                        ISNULL(State,1),
                        ISNULL(Note,''),
                        ISNULL(MothlyTimeId,0)
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_ParkingLotMonthly/Rows', 2)
    			With(MonthlyId Int,StartDate datetime,EndDate datetime,OperateDate datetime,Amount decimal(18,2),MonthlyType tinyint,MemberId int,
                    Opertator int,State tinyint,Note Nvarchar(1000),MothlyTimeId int)
                    
                Insert into @TmpMothlyLicensePlate
        		SELECT	ISNULL(SystemId,0),
                        ISNULL(MonthlyId,0),
                        ISNULL(LicensePlateNo,'')
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_MothlyLicensePlate/Rows', 2)
    			With(SystemId Int,MonthlyId Int,LicensePlateNo nvarchar(20))    
                
                Insert into @TmpMothlyGroup
        		SELECT	ISNULL(SystemId,0),
                        ISNULL(MonthlyId,0),
                        ISNULL(ParkingLotId,0)
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_MothlyGroup/Rows', 2)
    			With(SystemId Int,MonthlyId Int,ParkingLotId int)    
                
                Insert into @TmpChargePlan
        		SELECT	ISNULL(ChargePlanId,0),
                        ISNULL(ChargePlanName,''),
                        ISNULL(ChargePlanType,1),
                        ISNULL(PlanStartDate,GetDate()),
                        ISNULL(PlanEndDate,GetDate()),
                        ISNULL(CreateEmp,0),
                        CreateTime,
                        ISNULL(ModifyEmp,0),
                        ModifyDate,
                        ISNULL(State,1),
                        ISNULL(Note,'')
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_ChargePlan/Rows', 2)
    			With(ChargePlanId Int,ChargePlanName Nvarchar(30),ChargePlanType tinyint,PlanStartDate datetime,
                    PlanEndDate datetime,CreateEmp int,CreateTime datetime,ModifyEmp int,ModifyDate datetime,State tinyint,Note Nvarchar(1000))
    			
                
                Insert into @TmpChargeRule
        		SELECT	ISNULL(ChargeRuleId,0),
                        ISNULL(ChargeRuleName,''),
                        ISNULL(ReferencePriceNote,''),
                        ISNULL(LicensePlateType,1),
                        ISNULL(ChargeMode,0),
                        ISNULL(StartTime,0),
                        ISNULL(EndTime,0),
                        ISNULL(Freetype,0),
                        ISNULL(Freetime,0),
                        ISNULL(TimeLimit,0),
                        CreateTime,
                        ISNULL(CreateEmp,0),
                        ISNULL(ModifyEmp,0),
                        ModifyDate,
                        ISNULL(ChargeLimit,0),
                        ISNULL(AttachCharge,0),
                        ISNULL(State,1),
                        ISNULL(Note,''),
                        ISNULL(InStartTime,0),
                        ISNULL(InEndTime,0)
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_ChargeRule/Rows', 2)
    			With(ChargeRuleId Int,ChargeRuleName Nvarchar(30),ReferencePriceNote Nvarchar(30),LicensePlateType tinyint,ChargeMode tinyint,
                    StartTime int,EndTime int,Freetype tinyint,Freetime int,TimeLimit int,CreateTime datetime,CreateEmp int,ModifyEmp int,
                    ModifyDate datetime,ChargeLimit decimal(18,2),AttachCharge decimal(18,2),State tinyint,Note Nvarchar(1000),InStartTime int,InEndTime int)
                
                Insert into @TmpChargeRuleTime
        		SELECT	ISNULL(SystemId,0),
                        ISNULL(ChargeRuleId,0),
                        ISNULL(TimeType,1),
                        ISNULL(StartTime,0),
                        ISNULL(EndTime,0),
                        ISNULL(UnitTime,0),
                        ISNULL(UnitCharge,0),
                        ISNULL(Note,'')
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_ChargeRuleTime/Rows', 2)
    			With(SystemId Int,ChargeRuleId int,TimeType tinyint,StartTime int,EndTime nvarchar(30),UnitTime int,
                    UnitCharge decimal(9,2),Note Nvarchar(1000))    
                
                Insert into @TmpChargeConfig
        		SELECT	ISNULL(ChargeConfigId,0),
                        ISNULL(ChargePlanId,0),
                        ISNULL(ChargeRuleId,0),
                        ISNULL(ParkingLotId,0),
                        ISNULL(CarType,1),
                        ISNULL(CreateEmp,0),
                        CreateTime,
                        ISNULL(ModifyEmp,0),
                        ModifyDate,
                        ISNULL(State,1),
                        ISNULL(Note,'')
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_ChargeConfig/Rows', 2)
    			With(ChargeConfigId Int,ChargePlanId Int,ChargeRuleId Int,ParkingLotId Int,CarType tinyint,CreateEmp int,
                    CreateTime datetime,ModifyEmp int,ModifyDate datetime,State tinyint,Note Nvarchar(1000))
                
                Insert into @TmpUser
        		SELECT	ISNULL(UserId,0),
                        ISNULL(UserName,''),
                        ISNULL(EmpId,0),
                        ISNULL(EmpName,''),
                        ISNULL(Phone,''),
                        ISNULL(Email,''),
                        ISNULL(Password,''),
                        ISNULL(Note,'')
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_User/Rows', 2)
    			With(UserId Int,UserName Nvarchar(30),EmpId int,EmpName Nvarchar(30),Phone Nvarchar(30),
                    Email Nvarchar(30),Password Nvarchar(200),Note Nvarchar(1000))
                    
                Insert into @TmpUserParkingLot
        		SELECT	ISNULL(SystemId,0),
                        ISNULL(UserId,0),
                        ISNULL(ParkingLotId,0)
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_UserParkingLot/Rows', 2)
    			With(SystemId Int,UserId int,EmpId int,ParkingLotId int)
                
                Insert into @TmpWeekLimitTrafficWhiteList
        		SELECT	ISNULL(Id,0),
                        ISNULL(LimitTrafficWeek,0),
                        ISNULL(WhiteListId,0),
                        ISNULL(ParkingLotId,0),
                        ISNULL(LicensePlateType,0),
                        ISNULL(LicensePlateNo,N''),
                        ISNULL(RegisterDate,GetDate()),
                        ISNULL(OperateEmp,0),
                        ISNULL(Note,N'')
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_WeekLimitTrafficWhiteList/Rows', 2)
    			With(Id Int,LimitTrafficWeek tinyint,WhiteListId int,ParkingLotId int,LicensePlateType tinyint,LicensePlateNo nvarchar(20),RegisterDate datetime,OperateEmp int,Note nvarchar(1000))
                
                Insert into @TmpParkingLotMothlyTime
        		SELECT	ISNULL(MothlyTimeId,0),
                        ISNULL(ParkingLotId,0),
                        ISNULL(StartMinute,0),
                        ISNULL(EndMinute,0),
                        ISNULL(State,0),
                        ISNULL(CreateEmp,0),
                        ISNULL(CreateTime,GetDate()),
                        ISNULL(ModifyEmp,0),
                        ISNULL(ModifyDate,GetDate()),
                        ISNULL(Note,''),
                        ISNULL(ChargeRuleId,0)
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_ParkingLotMothlyTime/Rows', 2)
    			With(MothlyTimeId Int,ParkingLotId int,StartMinute int,EndMinute int,State int,
                    CreateEmp int,CreateTime datetime,ModifyEmp int,ModifyDate datetime,Note Nvarchar(1000),ChargeRuleId int)
                    
                Insert into @TmpCarGroup
        		SELECT	ISNULL(GroupId,0),
                        ISNULL(ParkingLotId,0),
                        ISNULL(GroupName,''),
                        ISNULL(PId,0),
                        ISNULL(PName,''),
                        ISNULL(BerthNum,0),
                        ISNULL(CreateEmp,0),
                        ISNULL(CreateDate,GetDate()),
                        ISNULL(ModifyEmp,0),
                        ISNULL(ModifyDate,GetDate()),
                        ISNULL(State,0),
                        ISNULL(Note,'')
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_CarGroup/Rows', 2)
    			With(GroupId Int,ParkingLotId int,GroupName nvarchar(30),PId int,PName nvarchar(500),
                    BerthNum int,CreateEmp int,CreateDate datetime,ModifyEmp int,ModifyDate datetime,State tinyint,Note Nvarchar(1000))    
                
                Insert into @TmpCarGroupLicensePlateNo
        		SELECT	ISNULL(SystemId,0),
                        ISNULL(GroupId,0),
                        ISNULL(LicensePlateNo,'')
    			FROM OPENXML(@I, '/IPS_ParkingLot/Rows/IPS_CarGroupLicensePlateNo/Rows', 2)
    			With(SystemId Int,GroupId int,LicensePlateNo nvarchar(20)) 
                    
                EXEC sp_xml_removedocument @I
                /***********************************************************/  
                
                Select @ParkingLotId = ParkingLotId From @TmpParkingLot
                
                Delete from IPS_ParkingLot Where ParkingLotId = @ParkingLotId
                Delete from IPS_WhiteList Where WhiteListId in (Select WhiteListId from IPS_WhiteListBindParkingLot Where ParkingLotId = @ParkingLotId)
                Delete from IPS_WhiteListBindParkingLot Where ParkingLotId = @ParkingLotId
                
                Delete from IPS_ParkingLotMonthly  Where MonthlyId in (Select MonthlyId from IPS_MothlyGroup where ParkingLotId = @ParkingLotId)
                Delete from IPS_MothlyLicensePlate Where MonthlyId in (Select MonthlyId from IPS_MothlyGroup where ParkingLotId = @ParkingLotId)
                Delete from IPS_MothlyGroup Where ParkingLotId = @ParkingLotId
                Delete from IPS_ChargePlan  Where ChargePlanId in (Select ChargePlanId from IPS_ChargeConfig where ParkingLotId = @ParkingLotId)
                Delete from IPS_ChargeRule  Where ChargeRuleId in (Select ChargeRuleId from IPS_ChargeConfig where ParkingLotId = @ParkingLotId)
                Delete from IPS_ChargeRuleTime  Where ChargeRuleId in (Select ChargeRuleId from IPS_ChargeConfig where ParkingLotId = @ParkingLotId)
                Delete from IPS_ChargeConfig   Where ParkingLotId = @ParkingLotId
                Delete from IPS_User Where UserId in (Select UserId From IPS_UserParkingLot where ParkingLotId = @ParkingLotId)
                Delete from IPS_UserParkingLot where ParkingLotId = @ParkingLotId
                Delete from IPS_WeekLimitTrafficWhiteList where ParkingLotId = @ParkingLotId
                Delete from IPS_ParkingLotMothlyTime where ParkingLotId = @ParkingLotId
                Delete from IPS_CarGroupLicensePlateNo where GroupId in (Select GroupId from IPS_CarGroup where ParkingLotId = @ParkingLotId)
                Delete from IPS_CarGroup where ParkingLotId = @ParkingLotId
                
                Insert into IPS_ParkingLot(ParkingLotId,ParkingLotNo,ParkingLotName,ParkingLotAddr,ParkingType,ChargeType,BerthNum,ParkingFee,ChargeEmp,RedTipNum,
                    YellowTipNum,FreeTime,Longitude,Latitude,Telphone,CreateDate,CreateEmp,ModifyDate,ModifyEmp,State,Note,OrganizationId)
                Select ParkingLotId,ParkingLotNo,ParkingLotName,ParkingLotAddr,ParkingType,ChargeType,BerthNum,ParkingFee,ChargeEmp,RedTipNum,
                    YellowTipNum,FreeTime,Longitude,Latitude,Telphone,CreateDate,CreateEmp,ModifyDate,ModifyEmp,State,Note,OrganizationId
                    from @TmpParkingLot
                
                --Insert into IPS_WhiteList
                --Select * from @TmpWhiteList
                
                Insert into @TmpWhiteList_Copy 
                Select WhiteListId from @TmpWhiteList 
                While (exists (select 1 from @TmpWhiteList_Copy))
                Begin
                    Select top 1 @WhiteListId = WhiteListId from @TmpWhiteList_Copy 
                    If not Exists(Select * From IPS_WhiteList where WhiteListId = @WhiteListId)
                        Insert into IPS_WhiteList
                        Select * from @TmpWhiteList where WhiteListId = @WhiteListId
                    DELETE @TmpWhiteList_Copy WHERE WhiteListId = @WhiteListId; 
                End 
                
                Insert into IPS_WhiteListBindParkingLot
                Select * from @TmpWhiteListBindParkingLot
                
                
                
                --Insert into IPS_ParkingLotMonthly
                --Select * from @TmpParkingLotMonthly
                --Insert into IPS_MothlyLicensePlate
                --Select * from @TmpMothlyLicensePlate
                --Insert into IPS_MothlyGroup
                --Select * from @TmpMothlyGroup
                
                
                Insert into @TmpMonthly_Copy 
                Select MonthlyId from @TmpParkingLotMonthly 
                While (exists (select 1 from @TmpMonthly_Copy))
                Begin
                    Select top 1 @MonthlyId = MonthlyId from @TmpMonthly_Copy 
                    If not Exists(Select * From IPS_ParkingLotMonthly where MonthlyId = @MonthlyId)
                        Insert into IPS_ParkingLotMonthly
                        Select * from @TmpParkingLotMonthly where MonthlyId = @MonthlyId
                    DELETE @TmpMonthly_Copy WHERE MonthlyId = @MonthlyId; 
                End  
                
                Insert into @TmpMothlyLicensePlate_Copy 
                Select SystemId from @TmpMothlyLicensePlate 
                While (exists (select 1 from @TmpMothlyLicensePlate_Copy))
                Begin
                    Select top 1 @SystemId = SystemId from @TmpMothlyLicensePlate_Copy 
                    If not Exists(Select * From IPS_MothlyLicensePlate where SystemId = @SystemId)
                        Insert into IPS_MothlyLicensePlate
                        Select * from @TmpMothlyLicensePlate where SystemId = @SystemId
                    DELETE @TmpMothlyLicensePlate_Copy WHERE SystemId = @SystemId; 
                End  
                
                Insert into @TmpMothlyGroup_Copy 
                Select SystemId from @TmpMothlyGroup 
                While (exists (select 1 from @TmpMothlyGroup_Copy))
                Begin
                    Select top 1 @SystemId = SystemId from @TmpMothlyGroup_Copy 
                    If not Exists(Select * From IPS_MothlyGroup where SystemId = @SystemId)
                        Insert into IPS_MothlyGroup
                        Select * from @TmpMothlyGroup where SystemId = @SystemId
                    DELETE @TmpMothlyGroup_Copy WHERE SystemId = @SystemId; 
                End  
                
                
                --Insert into IPS_ChargePlan
                --Select * from @TmpChargePlan
                --Insert into IPS_ChargeRule
                --Select * from @TmpChargeRule
                --Insert into IPS_ChargeRuleTime
                --Select * from @TmpChargeRuleTime
                
                Insert into @TmpChargePlan_Copy 
                Select ChargePlanId from @TmpChargePlan 
                While (exists (select 1 from @TmpChargePlan_Copy))
                Begin
                    Select top 1 @ChargePlanId = ChargePlanId from @TmpChargePlan_Copy 
                    If not Exists(Select * From IPS_ChargePlan where ChargePlanId = @ChargePlanId)
                        Insert into IPS_ChargePlan
                        Select * from @TmpChargePlan where ChargePlanId = @ChargePlanId
                    DELETE @TmpChargePlan_Copy WHERE ChargePlanId = @ChargePlanId; 
                End 
                
                Insert into @TmpChargeRule_Copy 
                Select ChargeRuleId from @TmpChargeRule 
                While (exists (select 1 from @TmpChargeRule_Copy))
                Begin
                    Select top 1 @ChargeRuleId = ChargeRuleId from @TmpChargeRule_Copy 
                    If not Exists(Select * From IPS_ChargeRule where ChargeRuleId = @ChargeRuleId)
                        Insert into IPS_ChargeRule
                        Select * from @TmpChargeRule where ChargeRuleId = @ChargeRuleId
                    DELETE @TmpChargeRule_Copy WHERE ChargeRuleId = @ChargeRuleId; 
                End 
                
                Insert into @TmpChargeRuleTime_Copy 
                Select SystemId from @TmpChargeRuleTime 
                While (exists (select 1 from @TmpChargeRuleTime_Copy))
                Begin
                    Select top 1 @SystemId = SystemId from @TmpChargeRuleTime_Copy 
                    If not Exists(Select * From IPS_ChargeRuleTime where SystemId = @SystemId)
                        Insert into IPS_ChargeRuleTime
                        Select * from @TmpChargeRuleTime where SystemId = @SystemId
                    DELETE @TmpChargeRuleTime_Copy WHERE SystemId = @SystemId; 
                End 
                
                
                Insert into IPS_ChargeConfig
                Select * from @TmpChargeConfig
                
                Insert into @TmpUser_Copy 
                Select * from @TmpUser 
                While (exists (select 1 from @TmpUser_Copy))
                Begin
                    Select top 1 @UserId = UserId from @TmpUser_Copy 
                    If not Exists(Select * From IPS_User where UserId = @UserId)
                        Insert into IPS_User
                        Select * from @TmpUser where UserId = @UserId
                    DELETE @TmpUser_Copy WHERE UserId = @UserId; 
                End  
                
                Insert into IPS_UserParkingLot
                Select * from @TmpUserParkingLot
                
                --插入白名单限行列表
                Insert into IPS_WeekLimitTrafficWhiteList
                Select * from @TmpWeekLimitTrafficWhiteList
                
                --更新场内包月车
                If Exists(Select B.* From IPS_ParkingLotMonthly_View A Inner Join IPS_Order B On A.LicensePlateNo = B.LicensePlateNo Where ChargeType = 10)
                    Update IPS_Order Set ChargeType = 30
                        From IPS_Order A Inner Join IPS_ParkingLotMonthly_View B On A.LicensePlateNo = B.LicensePlateNo Where ChargeType = 10
                --更新场内白名单
                If Exists(Select B.* From IPS_WhiteList A Inner Join IPS_Order B On A.LicensePlateNo = B.LicensePlateNo Where ChargeType = 10)
                    Update IPS_Order Set ChargeType = 20
                        From IPS_Order A Inner Join IPS_WhiteList B On A.LicensePlateNo = B.LicensePlateNo Where ChargeType = 10     
                        
                --插入停车场包月时段表
                Insert into  IPS_ParkingLotMothlyTime(MothlyTimeId,ParkingLotId,StartMinute,EndMinute,State,CreateEmp,CreateTime,ModifyEmp,ModifyDate,Note,ChargeRuleId)  
                Select MothlyTimeId,ParkingLotId,StartMinute,EndMinute,State,CreateEmp,CreateTime,ModifyEmp,ModifyDate,Note,ChargeRuleId from  @TmpParkingLotMothlyTime     
                
                --插入车辆分组表
                Insert into IPS_CarGroup(GroupId,ParkingLotId,GroupName,PId,PName,BerthNum,CreateEmp,CreateDate,ModifyEmp,ModifyDate,State,Note)
                Select GroupId,ParkingLotId,GroupName,PId,PName,BerthNum,CreateEmp,CreateDate,ModifyEmp,ModifyDate,State,Note from @TmpCarGroup
                --插入分组车牌表
                Insert into IPS_CarGroupLicensePlateNo(SystemId,GroupId,LicensePlateNo)
                Select SystemId,GroupId,LicensePlateNo from @TmpCarGroupLicensePlateNo
                 
            End
            
        Commit Tran IPS_SyncParkingLotBaseDataTran           
    End Try
	Begin Catch
		Set @ErrorID = 1001
	End Catch
  
    --错误回滚事务
	If @@TRANCOUNT > 0
	Begin
		Set @ErrorID = 1002
		
		RollBack Tran IPS_SyncParkingLotBaseDataTran
	End

	Return @ErrorID
End
GO
USE [master]
GO
ALTER DATABASE [EduSystem] SET  READ_WRITE 
GO
