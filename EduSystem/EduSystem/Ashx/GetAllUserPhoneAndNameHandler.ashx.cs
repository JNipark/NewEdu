﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using SqlServer;
using SqlServer.Models;
using Newtonsoft.Json;
using Newtonsoft;

namespace EduSystem.Ashx
{
    /// <summary>
    /// GetAllUserPhoneAndNameHandler 的摘要说明
    /// </summary>
    public class GetAllUserPhoneAndNameHandler : IHttpHandler
    {
        DbBase db = new DbBase();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            List<SelectOption> options = new List<SelectOption>();
            try
            {
                db.Open();
                List<tb_UserInfo> userInfos = new List<tb_UserInfo>();
                db.SelectAllUserInfo(out userInfos);
                SelectOption option = new SelectOption();
                for (int i = 0; i < userInfos.Count; i++)
                {
                    option = new SelectOption();
                    option.name = userInfos[i].UserName+"-"+userInfos[i].PhoneNumber;
                    options.Add(option);
                }
                context.Response.Write(JsonConvert.SerializeObject(options));
            }
            catch (Exception)
            {

                throw;
            }
            finally {
                db.Close();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}