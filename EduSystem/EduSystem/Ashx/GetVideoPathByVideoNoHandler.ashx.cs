﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SqlServer;
using SqlServer.Models;
using Newtonsoft.Json;
using Newtonsoft;
using System.Security.Cryptography;
using System.Web.Script.Serialization;
using System.IO;

namespace EduSystem.Ashx
{
    /// <summary>
    /// GetVideoPathByVideoNoHandler 的摘要说明
    /// </summary>
    public class GetVideoPathByVideoNoHandler : IHttpHandler
    {
        DbBase db = new DbBase();
        List<string> ls1 = new List<string>() { "yes" };
        List<string> ls2 = new List<string>() { "no" };
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                if (db.Open())
                {
                    var data = context.Request;
                    var stream = new StreamReader(data.InputStream).ReadToEnd();
                    var msgData = new JavaScriptSerializer().Deserialize<tb_VideoInfo>(stream);
                    List<tb_VideoInfo> videoInfos = new List<tb_VideoInfo>();
                    db.SelectVideoInfoByVideoNo(msgData.VideoNo, out videoInfos);
                    if (videoInfos.Count>0)
                    {
                        string path = videoInfos[0].VideoPath;
                        if (!string.IsNullOrEmpty(path))
                        {
                            ls1.Add(path);
                            context.Response.Write(new JavaScriptSerializer().Serialize(ls1));
                        }
                        else
                        {
                            context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
                        }
                    }
                    else
                    {
                        context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
                    }
                }
            }
            catch (Exception)
            {
                context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
            }
            finally {
                db.Close();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}