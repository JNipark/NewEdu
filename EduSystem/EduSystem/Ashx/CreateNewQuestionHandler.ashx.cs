﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using SqlServer;
using SqlServer.Models;

namespace EduSystem.Ashx
{
    /// <summary>
    /// CreateNewQuestionHandler 的摘要说明
    /// </summary>
    public class CreateNewQuestionHandler : IHttpHandler
    {
        DbBase db = new DbBase();
        List<string> ls1 = new List<string>() { "yes" };
        List<string> ls2 = new List<string>() { "no" };
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            List<tb_UserVideo> userVideos = new List<tb_UserVideo>();
            tb_UserVideo userVideo = new tb_UserVideo();
            try
            {
                db.Open();
                var data = context.Request;
                var stream = new StreamReader(data.InputStream).ReadToEnd();
                var msgData = new JavaScriptSerializer().Deserialize<tb_Question>(stream);
                //保存到服务器        
                if (db.InsertNewQuestion(msgData))
                {
                    //遍历已有用户，添加
                    List<tb_UserInfo> userInfos = new List<tb_UserInfo>();
                    if (db.SelectAllUserInfo(out userInfos))
                    {
                        for (int i = 0; i < userInfos.Count; i++)
                        {
                            userVideo = new tb_UserVideo();
                            userVideo.UserPhone = userInfos[i].PhoneNumber;
                            userVideo.VideoNo = msgData.VideoNo;
                            userVideo.Statement = "否";
                            userVideo.QuestionNo = msgData.Question;
                            userVideo.QuestionAns = msgData.RightAnswer;
                            userVideo.UpdateTime = DateTime.Now;
                            userVideos.Add(userVideo);
                        }
                        if (db.InsertUserVideos(userVideos))
                        {
                            context.Response.Write(new JavaScriptSerializer().Serialize(ls1));
                        }
                        else
                        {
                            context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
                        }
                    }
                    else
                    {
                        context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
                    }
                }
            }
            catch (Exception ex)
            {
                string longex = ex.Message;
                context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
            }
            finally
            {
                db.Close();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}