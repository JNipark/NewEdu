﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using SqlServer;
using SqlServer.Models;
using Newtonsoft.Json;
using Newtonsoft;

namespace EduSystem.Ashx
{
    public class SelectOption
    {
        public string name { get; set; }
    }
    /// <summary>
    /// GetAllVideoNoHandler 的摘要说明
    /// </summary>
    public class GetAllVideoNoHandler : IHttpHandler
    {
        DbBase db = new DbBase();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            List<SelectOption> options = new List<SelectOption>();
            try
            {
                db.Open();
                List<tb_VideoInfo> videoInfos = new List<tb_VideoInfo>();
                db.SelectAllVideoInfo(out videoInfos);
                SelectOption option = new SelectOption();
                for (int i = 0; i < videoInfos.Count; i++)
                {
                    option = new SelectOption();
                    option.name = videoInfos[i].VideoNo;
                    options.Add(option);
                }
                context.Response.Write(JsonConvert.SerializeObject(options));
            }
            catch (Exception)

            {
                context.Response.Write(JsonConvert.SerializeObject(options));
            }
            finally
            {
                db.Close();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}