﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using SqlServer;
using SqlServer.Models;
using Newtonsoft.Json;
using Newtonsoft;

namespace EduSystem.Ashx
{
    /// <summary>
    /// UpdateUserVideoStatementHandler 的摘要说明
    /// </summary>
    public class UpdateUserVideoStatementHandler : IHttpHandler
    {
        DbBase db = new DbBase();
        List<string> ls1 = new List<string>() { "yes" };
        List<string> ls2 = new List<string>() { "no" };
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                db.Open();
                var data = context.Request;
                var stream = new StreamReader(data.InputStream).ReadToEnd();
                var msgData = new JavaScriptSerializer().Deserialize<tb_UserVideo>(stream);
                List<tb_UserVideo> userVideos = new List<tb_UserVideo>();

                if (db.SelectUserVideoByUserPhoneAndVideoNo(msgData.UserPhone, msgData.VideoNo, out userVideos))
                {
                    if (userVideos.Count > 0)
                    {
                        msgData = userVideos[0];
                        msgData.Statement = "是";
                        msgData.UpdateTime = DateTime.Now;
                        db.UpdateUserVideo(msgData);
                        context.Response.Write(new JavaScriptSerializer().Serialize(ls1));
                    }
                    else
                    {
                        context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
                    }
                }
                else
                {
                    context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
                }
            }
            catch (Exception)
            {
                context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
            }
            finally
            {
                db.Close();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}