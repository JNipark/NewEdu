﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using SqlServer;
using SqlServer.Models;
using Newtonsoft.Json;
using Newtonsoft;

namespace EduSystem.Ashx
{
    /// <summary>
    /// GetFromDataHandler 的摘要说明
    /// </summary>
    public class GetFromDataHandler : IHttpHandler
    {
        DbBase db = new DbBase();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            var data = context.Request;
            var stream = new StreamReader(data.InputStream).ReadToEnd();
            var msgData = new JavaScriptSerializer().Deserialize<tb_UserVideo>(stream);
            List<tb_UserVideo> userVideos = new List<tb_UserVideo>();
            try
            {
                db.Open();
                if (string.IsNullOrEmpty(msgData.UserPhone) && !string.IsNullOrEmpty(msgData.VideoNo))
                {
                    db.SelectUserVideoByVideoNo(msgData.VideoNo, out userVideos);
                    for (int i = 0; i < userVideos.Count; i++)
                    {
                        userVideos[i].UserPhone = GetUserNameByPhone(userVideos[i].UserPhone);
                    }

                }
                else if (!string.IsNullOrEmpty(msgData.UserPhone) && string.IsNullOrEmpty(msgData.VideoNo))
                {
                    db.SelectUserVideoByUserPhone(msgData.UserPhone, out userVideos);
                    for (int i = 0; i < userVideos.Count; i++)
                    {
                        userVideos[i].UserPhone = GetUserNameByPhone(userVideos[i].UserPhone);
                    }
                }
                else if (!string.IsNullOrEmpty(msgData.UserPhone) && !string.IsNullOrEmpty(msgData.VideoNo))
                {
                    db.SelectUserVideoByUserPhoneAndVideoNo(msgData.UserPhone, msgData.VideoNo, out userVideos);
                    for (int i = 0; i < userVideos.Count; i++)
                    {
                        userVideos[i].UserPhone = GetUserNameByPhone(userVideos[i].UserPhone);
                    }
                }
                var data1 = new
                {
                    data = userVideos
                };
                context.Response.Write(JsonConvert.SerializeObject(data1, new JsonSerializerSettings
                {
                    DateFormatString = "yyyy-MM-dd HH:mm:ss"
                }));

            }
            catch (Exception)
            {
                var data1 = new
                {
                    data = userVideos
                };
                context.Response.Write(JsonConvert.SerializeObject(data1, new JsonSerializerSettings
                {
                    DateFormatString = "yyyy-MM-dd HH:mm:ss"
                }));
            }
            finally
            {
                db.Close();
            }
        }

        public String GetUserNameByPhone(string phone)
        {
            List<tb_UserInfo> userInfos = new List<tb_UserInfo>();
            db.SelectUserInfoByPhone(phone, out userInfos);
            return userInfos[0].UserName;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}