﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using SqlServer;
using SqlServer.Models;
using Newtonsoft.Json;
using Newtonsoft;

namespace EduSystem.Ashx
{
    /// <summary>
    /// GetQuesiontByVideoNoHandler 的摘要说明
    /// </summary>
    public class GetQuesiontByVideoNoHandler : IHttpHandler
    {
        DbBase db = new DbBase();
        List<string> ls1 = new List<string>() { "yes" };
        List<string> ls2 = new List<string>() { "no" };
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                db.Open();
                var data = context.Request;
                var stream = new StreamReader(data.InputStream).ReadToEnd();
                var msgData = new JavaScriptSerializer().Deserialize<tb_Question>(stream);
                List<tb_Question> questions = new List<tb_Question>();
                db.SelectQuestionByVideoNo(msgData.VideoNo, out questions);
                if (questions.Count>0)
                {
                    ls1.Add(questions[0].Question);
                    ls1.Add(questions[0].AnswerA);
                    ls1.Add(questions[0].AnswerB);
                    ls1.Add(questions[0].AnswerC);
                    ls1.Add(questions[0].RightAnswer);
                    context.Response.Write(new JavaScriptSerializer().Serialize(ls1));
                }
                else
                {
                    context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
                }
            }
            catch (Exception)
            {
                context.Response.Write(new JavaScriptSerializer().Serialize(ls2));
            }
            finally
            {
                db.Close();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}