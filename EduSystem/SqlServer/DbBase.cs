﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlSugar;
using SqlServer.Models;

namespace SqlServer
{
    public class DbBase
    {
        //string _connectionString = "Server=47.98.111.17;Database=EduSystem;User ID = zqSql; Password=Zq1993102;MultipleActiveResultSets=true";
        string _connectionString = "Server=127.0.0.1;Database=EduSystem;User ID=zqSql; Password=Zq1993102;MultipleActiveResultSets=true";

        SqlSugarClient db;

        #region "Open & Close"
        public bool Open()
        {
            try
            {
                db = new SqlSugarClient(new ConnectionConfig()
                {
                    ConnectionString = _connectionString,
                    DbType = SqlSugar.DbType.SqlServer,
                    IsAutoCloseConnection = true,
                    InitKeyType = InitKeyType.Attribute
                });
                db.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Close()
        {
            try
            {
                db.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region "Insert"
        public bool InsertNewUser(tb_UserInfo userInfo)
        {
            try
            {
                int a = db.Insertable(userInfo).ExecuteReturnIdentity();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool InsertNewSession(tb_Session session)
        {
            try
            {
                int a = db.Insertable(session).ExecuteReturnIdentity();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool InsertVideoInfo(tb_VideoInfo videoInfo)
        {
            try
            {
                int a = db.Insertable(videoInfo).ExecuteReturnIdentity();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool InsertNewQuestion(tb_Question question)
        {
            try
            {
                int a = db.Insertable(question).ExecuteReturnIdentity();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool InsertUserVideos(List<tb_UserVideo> userVideos)
        {
            try
            {
                int a = db.Insertable(userVideos).ExecuteReturnIdentity();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region "Delete"
        #endregion

        #region "Update"
        public bool UpdateUserVideo(tb_UserVideo userVideo)
        {
            try
            {
                //var a = db.Updateable(weeklyPriceAndType).WhereColumns(it => it.UserPhone == weeklyPriceAndType.UserPhone).ExecuteCommand();
                var b = db.Updateable(userVideo).Where(it => it.UserPhone == userVideo.UserPhone && it.VideoNo == userVideo.VideoNo).ExecuteCommand();
                return true;
            }
            catch (Exception ex)
            {
                string a = ex.Message;
                return false;
            }
        }
        #endregion

        #region "Select"
        public bool SelectUserInfoByPhone(string phone, out List<tb_UserInfo> userInfos)
        {
            userInfos = new List<tb_UserInfo>();
            try
            {
                userInfos = db.Queryable<tb_UserInfo>().Where(it => it.PhoneNumber == phone).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool SelectUserVideoByVideoNo(string videoNo, out List<tb_UserVideo> userVideos)
        {
            userVideos = new List<tb_UserVideo>();
            try
            {
                userVideos = db.Queryable<tb_UserVideo>().Where(it => it.VideoNo == videoNo).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool SelectUserVideoByUserPhone(string phone, out List<tb_UserVideo> userVideos)
        {
            userVideos = new List<tb_UserVideo>();
            try
            {
                userVideos = db.Queryable<tb_UserVideo>().Where(it => it.UserPhone == phone).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SelectUserVideoByUserPhoneAndVideoNo(string phone, string videoNo, out List<tb_UserVideo> userVideos)
        {
            userVideos = new List<tb_UserVideo>();
            try
            {
                userVideos = db.Queryable<tb_UserVideo>().Where(it => it.UserPhone == phone && it.VideoNo == videoNo).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SelectQuestionByVideoNo(string videoNo, out List<tb_Question> questions)
        {
            questions = new List<tb_Question>();
            try
            {
                questions = db.Queryable<tb_Question>().Where(it => it.VideoNo == videoNo).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SelectAllVideoInfo(out List<tb_VideoInfo> videoInfos)
        {
            videoInfos = new List<tb_VideoInfo>();
            try
            {
                videoInfos = db.Queryable<tb_VideoInfo>().ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SelectAllUserInfo(out List<tb_UserInfo> userInfos)
        {
            userInfos = new List<tb_UserInfo>();
            try
            {
                userInfos = db.Queryable<tb_UserInfo>().ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SelectAllQuestions(out List<tb_Question> questions)
        {
            questions = new List<tb_Question>();
            try
            {
                questions = db.Queryable<tb_Question>().ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SelectSessionBySession(string session, out tb_Session Session)
        {
            Session = new tb_Session();
            List<tb_Session> Sessions = new List<tb_Session>();
            try
            {
                Sessions = db.Queryable<tb_Session>().Where(it => it.Session == session).ToList();
                Session = Sessions[0];
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SelectVideoInfoByVideoNo(string session, out List<tb_VideoInfo> videoInfos)
        {
            videoInfos = new List<tb_VideoInfo>();
            try
            {
                videoInfos = db.Queryable<tb_VideoInfo>().Where(it => it.VideoNo == session).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SelectVideosByPhone(string phone, out List<tb_UserVideo> userVideos)
        {
            userVideos = new List<tb_UserVideo>();
            try
            {
                userVideos = db.Queryable<tb_UserVideo>().Where(it => it.UserPhone == phone).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
